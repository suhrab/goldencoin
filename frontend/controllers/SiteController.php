<?php
namespace frontend\controllers;

use common\models\Blog;
use common\models\Portfolio;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout'    => 'error'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $portfolio = Portfolio::find()->limit(3)->all();
        return $this->render('index',compact('portfolio'));
    }
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionCase($url){
        $portfolio = Portfolio::findOne(['url'=>$url]);
        if ($portfolio){
            if ($portfolio->description_SEO)
                \Yii::$app->view->registerMetaTag([
                    'name' => 'description',
                    'content' => $portfolio->description_SEO
                ]);
            if ($portfolio->keywords_SEO)
                \Yii::$app->view->registerMetaTag([
                    'name' => 'keywords',
                    'content' => $portfolio->keywords_SEO
                ]);
            $this->view->title = $portfolio->title_SEO ? $portfolio->title_SEO : $portfolio->title;
            return $this->render('case_item',compact('portfolio'));
        } else {
            throw new NotFoundHttpException();
        }
    }
    public function actionCases($category)
    {
        $query = Portfolio::find()->where(['category'=>$category]);
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize'  => 8,
        ]);
        $pages->pageSizeParam = false;
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render(
            'case',
            compact('models','pages')
        );
    }
    public function actionBlog()
    {
        $query = Blog::find()->orderBy('created_at DESC');
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize'  => 5,
        ]);
        $pages->pageSizeParam = false;
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('blog',compact('pages','models'));
    }
    public function actionBlogItem($url)
    {
        $blog = Blog::findOne(['url'=>$url]);

        if ($blog)
        {
            if ($blog->description_SEO)
                \Yii::$app->view->registerMetaTag([
                    'name' => 'description',
                    'content' => $blog->description_SEO
                ]);
            if ($blog->keywords_SEO)
                \Yii::$app->view->registerMetaTag([
                    'name' => 'keywords',
                    'content' => $blog->keywords_SEO
                ]);
            $this->view->title = $blog->title_SEO ? $blog->title_SEO : $blog->title;
            return $this->render('blog_item',compact('blog'));
        }
        else
            throw new NotFoundHttpException();
    }
    public function actionReviews()
    {
        return $this->render('reviews');
    }
    public function actionContacts()
    {
        $this->layout = 'contact';
        return $this->render('contact');
    }

}
