$(document).scroll(function(){
    if (window.pageYOffset > '200')
    {
      $('.fixed-menu-top').addClass('sticked');
    }
    else
    {
      $('.fixed-menu-top').removeClass('sticked');
    }
});

$(document).ready(function(){
    $('.ajax-form').submit(function(e)
    {
      e.preventDefault();
      $(this).html('<h2 class="success">Спасибо! Ваша заявка успешно отправлена!</h2>');
    });
    $('.burger-menu-top,.close-menu').click(function()
    {
        $('.fixed-menu').toggleClass('open');
    });

    $('.phone-mask').mask('+7 (999) 999-99-99')

    $('.faq-question').click(function()
    {
      $(this).parent().toggleClass('open');
      $(this).siblings('.faq-answer').slideToggle();
    });
    $('.lazyYT').lazyYT();
    $('.slick-slider-portfolio').slick({
      arrows:false,
    });
    $('.portfolio-images').slick({
      prevArrow: '<div class="prev-slider"></div>',
      nextArrow: '<div class="next-slider"></div>',
        draggable:false,
    });
    $('.slick-reviews').slick({
      prevArrow: '<div class="prev-slider"></div>',
      nextArrow: '<div class="next-slider"></div>',
      slidesToShow: 2,
    });
    $('.slick-video').slick({
      prevArrow: '<div class="prev-slider"></div>',
      nextArrow: '<div class="next-slider"></div>',
    });
    $('.video-reviews-page').slick({
      slidesToShow: 2,
      slidesToScroll: 2,
      prevArrow: '<div class="prev-slider"></div>',
      nextArrow: '<div class="next-slider"></div>',
      responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow:1
        }
      },]
    });
    $('.team-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      prevArrow: '<div class="prev-slider"></div>',
      nextArrow: '<div class="next-slider"></div>',
      responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow:1
            }
          },
        ]
    });
    $('.client-slider').slick({
      prevArrow: '<div class="prev-slider"></div>',
      nextArrow: '<div class="next-slider"></div>',
      slidesToShow: 5,
       responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow:3
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow:1
            }
          }
        ]
      });
});