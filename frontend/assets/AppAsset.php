<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/animate.css',
        'https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap',
        'css/slick.css',
        'css/jquery.fancybox.min.css',
        'css/style.css',
        'css/jquery.fancybox.min.css',
        'css/lazyYT.css',
    ];
    public $js = [
        '/js/wow.min.js',
        '/js/bootstrap.min.js',
        '/js/slick.min.js',
        '/js/jquery.fancybox.min.js',
        '/js/lazyYT.js',
        '/js/jquery.mask.min.js',
        '/js/common.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
