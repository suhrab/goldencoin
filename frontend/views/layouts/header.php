<div class="fixed-menu">
    <div class="wrapper-menu">
        <div class="menu-items">
            <div>
                <a href="/">Главная</a>
            </div>
            <div>
                <a href="/about">О нас</a>
            </div>
            <div>
                <a href="/cases/1">Кейсы</a>
            </div>
            <div>
                <a href="/reviews">Отзывы</a>
            </div>
            <div>
                <a href="/blog">Блог</a>
            </div>
            <div>
                <a href="/contacts">Контакты</a>
            </div>
        </div>
    </div>
    <div class="close-menu">
        <img src="/img/close_img.png" alt="">
    </div>
</div>
<div class="fixed-menu-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-7 col-left-header">
                <div class="burger-menu-top">
                    <img src="/img/burger.png" alt="">
                </div>
                <a href="/" class="logo">
                    <img src="/img/logo.png" alt="">
                </a>
            </div>
            <div class="col-lg-6 col-5">
                <div class="right-tools-header">
                    <a class="number-menu-top" href="tel:+7 727 329 45 39">
                        +7 727 329 45 39
                    </a>
                    <button class="gradient-btn button" data-toggle="modal" data-target="#modal-contact">
                        <span>Обратный звонок</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>