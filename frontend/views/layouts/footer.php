
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="logo-footer bebas">
						<span class="yellow bebas">GLODEN</span> COIN
					</div>
					<div class="address-footer">
						Алматы, ул.досмухамедова, 89 офис 211
					</div>
					<a href="tel:87273294539" class="phone-footer">
						+7 727 329 45 35
					</a>
					<a class="mail-footer" href="mailto:info@goldencoin.kz">
						info@goldencoin.kz
					</a>
				</div>
				<div class="col-lg-6">
					<div class="footer-dev">
						<span>ПРОЕКТ</span>
						<a href="https://netlight.kz" target="_blank">
							<img src="/img/netlight.png" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<div class="modal fade" tabindex="-1" role="dialog" id="modal-contact" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<img src="/img/close_img.png" alt="">
				</button>
				<div class="title-modal">
					Отправить заявку
				</div>
				<form action="" class="form-style ajax-form">
					<div class="col-form" >
						<div class="wrapper-input"> 
							<input placeholder="Ваше имя" class="input-style" type="text" name="name">
						</div>
					</div>
					<div class="col-form" >
						<div class="wrapper-input">
							<input placeholder="Ваш сайт" class="input-style" type="text" name="phone">
						</div>
					</div>
					<div class="col-form" >
						<div class="wrapper-input">
							<input placeholder="Ваш телефон" class="input-style" type="text" name="phone">
						</div>
					</div>
					<div class="col-form">
						<div class="wrapper-input wrapper-input-submit">
							<button type="submit">
								Получить коммерческое
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
<?
$this->registerJs('
        new WOW().init();
        ');
