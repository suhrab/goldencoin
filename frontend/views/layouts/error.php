<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> | GOLDEN COIN </title>
    <?php $this->head() ?>
</head>
<body class="page404">
<?php $this->beginBody() ?>.

	<div class="fixed-menu-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-7 col-left-header">
					<div class="burger-menu-top">
						<img src="/img/burger.png" alt="">
					</div>
					<a href="" class="logo">
						<img src="img/logo_black.png" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>	
	<div id="error404">
		<img src="/img/404.png" class="animated zoomIn" alt="">
			<div class="title-error">
				Страница не найдена
			</div>
			<a class="blue-gradient-btn" href="/">Назад</a>
	</div>

<?php $this->endBody() ?>.
</body>
</html>
<?php $this->endPage() ?>
