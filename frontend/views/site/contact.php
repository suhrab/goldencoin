<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-wrapper">
    <div class="container">
        <h1 class="title-page">
            Контакты
        </h1>
        <div class="row">
            <div class="col-md-6 contacts">
                <div class="contacts-page">
                    <div class="title-contact">
                        Адрес:
                    </div>
                    <div class="content-contact">
                        Казахстан, Алматы  <br>
                        ул. Досмухамедова, 89  офис 211 <br>
                        угол ул. Жамбыла
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="title-contact">
                                Телефоны:
                            </div>
                            <div class="content-contact">
                                <a href="tel:+7 727 329 4539">+7 727 329 4539</a>
                                <a href="tel:+7 775 820 4617">+7 775 820 4617</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="title-contact">
                                Почта:
                            </div>
                            <div class="content-contact">
                                <a href="mailto:info@nasa.kz">info@nasa.kz</a>
                                <a href="mailto:info@netlight.kz">info@netlight.kz</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A1cc44ee4a414eac0610b6cf882f72f9f898410ef19f19d435f99044cc0c593d4&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div>
    </div>
    <div class="copyright">(с) Все права защищены</div>
</div>