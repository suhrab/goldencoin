
	<div class="page-wrapper">
		<div class="container">
			<h1 class="title-page">
                Блог
			</h1>
			<ul class="breadcrumb">
				<li>
					<a href="/">Главная</a>
				</li>
				<li>
					<a href="/blog">Блог</a>
				</li>
				<li>
					<span>
                        <?= $blog->title ?>
					</span>
				</li>
			</ul>
			<div class="back-to-blog">
				<a href="/blog" class="btn skewed-button yellow-button">
						<span>< Все статьи</span>
					</a>
			</div>
			<div class="blog-wrapper">
				<div class="blog-header">
					<div class="blog-title">
                        <?= $blog->title ?>
					</div>
					<div class="blog-year">
						<?= date('d.m.Y',$blog->created_at) ?>
					</div>
				</div>
				<div class="blog-content">
                    <?= $blog->description ?>
				</div>
			</div>
			<div class="text-right">
				<a href="/blog" class="btn skewed-button yellow-button">
						<span>< Все статьи</span>
					</a>
			</div>
		</div>
	</div>
    <?= $this->render('/layouts/form') ?>