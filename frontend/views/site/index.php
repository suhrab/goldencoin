<?php

/* @var $this yii\web\View */

$this->title = 'Главная';
?>
<header class="main-header">
    <div class="container">
        <h1>
            ПОДВИЖЕНИЕ<br>
            САЙТОВ В <span class="yellow">ТОП</span><br>
            ЯНДЕКС И GOOGLE
        </h1>
        <div class="description-header">
            Гарантированно вернем деньги, если <br>
            не устроит наша работа!
        </div>
        <button class="gradient-btn button">
            <span>Просчитать стоимость <br> продвижения</span>
        </button>
    </div>
</header>
<div id="portfolio" class="default-block">
    <div class="container">
        <div class="title-section">
            <div class="title-number wow fadeInLeft">
                9
            </div>
            <div class="title-content">
                <span class="yellow">лет опыта</span> <br>
                продвижения
            </div>
        </div>
        <div class="slick-slider-portfolio">
            <? foreach ($portfolio as $key => $item): ?>
            <div class="item-portfolio">
                <div class="row">
                    <div class="col-lg-6 wow fadeInUp" data-wow-duration="0.5s">
                        <div class="subtitle-content">
                            Кейс <?= $key+1 ?>: <span class="yellow"> <?= $item->title ?></span>
                        </div>
                        <div class="portfolio-table">
                            <div class="portfolio-table-item">
                                <div class="portfolio-table-item-label">
                                    Сайт
                                </div>
                                <div class="portfolio-table-item-content">
                                    <?= $item->site_url ?>
                                </div>
                            </div>
                            <div class="portfolio-table-item">
                                <div class="portfolio-table-item-label">
                                    Тема
                                </div>
                                <div class="portfolio-table-item-content">
                                    <?= $item->theme ?>
                                </div>
                            </div>
                            <div class="portfolio-table-item">
                                <div class="portfolio-table-item-label">
                                    Было
                                </div>
                                <div class="portfolio-table-item-content-number">
                                    <div class="yellow"><?= $item->before ?></div>
                                    пользователей <br> в день
                                </div>
                            </div>
                            <div class="portfolio-table-item">
                                <div class="portfolio-table-item-label">
                                    Стало
                                </div>
                                <div class="portfolio-table-item-content-number">
                                    <div class="yellow">
                                        <?= $item->after ?>
                                    </div>
                                    пользователей <br> в день
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-total">
                            <div class="portfolio-total-label">
                                Итого рост посетителей
                            </div>
                            <div class="yellow portfolio-total-number">
                                <?= $item->total ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 wow fadeInUp" data-wow-duration="0.8s">
                        <div class="portfolio-images">
                            <? if (unserialize($item->images)): ?>
                                <? foreach (unserialize($item->images) as $image): ?>
                                    <div class="portfolio-image">
                                        <a href="/uploads/portfolio/<?= $image ?>" data-fancybox="Портфолио">
                                            <img src="/uploads/portfolio/<?= $image ?>" alt="">
                                        </a>
                                    </div>
                             <? endforeach; ?>
                            <? endif ?>
                        </div>
                    </div>
                </div>
            </div>
            <? endforeach ?>
        </div>
        <div class="text-center">
            <a href="/cases" class="btn skewed-button yellow-button">
                <span>Все кейсы</span>
            </a>
        </div>
        <div class="star">
            <img src="/img/patrick.png" class="wow fadeInRightBottom" data-wow-delay="1s" alt="">
        </div>
        <div class="medusa wow fadeInUp" data-wow-delay="1s">
            <img src="/img/medusa.png" alt="">
        </div>
    </div>
</div>
<div id="services" class="default-block">
    <div class="container">
        <div class="title-section">
            <div class="title-number wow fadeInLeft">
                6
            </div>
            <div class="title-content">
                <span class="yellow">бесплатных услуг</span> <br>
                входящие в список работ
            </div>
        </div>
        <div class="row service-items">
            <div class="col-lg-6">
                <div class="service-item">
                    <div class="service-number yellow wow fadeInUp" data-wow-delay="0.4s">
                        01
                    </div>
                    <div class="service-text  wow fadeInUp" data-wow-delay="0.55s">
                        Комплексный аудит  <br>
                        сайта
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="service-item">
                    <div class="service-number yellow wow fadeInUp" data-wow-delay="0.55s">
                        02
                    </div>
                    <div class="service-text  wow fadeInUp" data-wow-delay="0.6s">
                        Маркетинговый <br>
                        анализ
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="service-item">
                    <div class="service-number yellow wow fadeInUp" data-wow-delay="0.60s">
                        03
                    </div>
                    <div class="service-text  wow fadeInUp" data-wow-delay="0.65s">
                        Разработка стратегии <br>
                        развития сайта
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="service-item">
                    <div class="service-number yellow wow fadeInUp" data-wow-delay="0.4s">
                        04
                    </div>
                    <div class="service-text  wow fadeInUp" data-wow-delay="0.45s">
                        Техническая оптимизация <br>
                        сайта
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="service-item">
                    <div class="service-number yellow wow fadeInUp" data-wow-delay="0.55s">
                        05
                    </div>
                    <div class="service-text  wow fadeInUp" data-wow-delay="0.45s">
                        Оптимизация <br>
                        контента сайта
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="service-item">
                    <div class="service-number yellow wow fadeInUp" data-wow-delay="0.6s">
                        06
                    </div>
                    <div class="service-text  wow fadeInUp" data-wow-delay="0.65s">
                        Внешняя оптимизация <br>
                        сайта
                    </div>
                </div>
            </div>
        </div>
        <div class="vodolaz wow swimRight" data-wow-delay="1s">
            <img src="/img/vodolaz.png" alt="">
        </div>
    </div>
    <div class="shark wow swim" data-wow-duration="2s" data-wow-delay="1s">
        <img src="/img/shark.png" alt="">
    </div>
</div>
<div id="steps" class="default-block">
    <div class="container">
        <div class="title-section">
            <div class="title-content">
                Как мы продвигаем
            </div>
        </div>
        <div class="row row-steps">
            <div class="col-lg-4">
                <div class="step-item wow fadeIn" data-wow-delay=".3s">
                    <div class="step-star">
                        <img src="/img/steps/star.png" alt="">
                    </div>
                    <div class="star-text">
                        Составляем <span>популярные</span><br>
                        запросы
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="step-item wow fadeIn" data-wow-delay=".4s">
                    <div class="step-star">
                        <img src="/img/steps/star1.png" alt="">
                    </div>
                    <div class="star-text">
                        <span>Оптимизирум</span> <br>
                        сайт
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="step-item wow fadeIn" data-wow-delay=".5s">
                    <div class="step-star">
                        <img src="/img/steps/star2.png" alt="">
                    </div>
                    <div class="star-text">
                        Пишем статьи со <br>
                        <span>100% уникальностью</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="step-item wow fadeIn" data-wow-delay=".6s">
                    <div class="step-star">
                        <img src="/img/steps/star3.png" alt="">
                    </div>
                    <div class="star-text">
                        Закупка трастовых<br>
                        ссылок на <span>проверенных<br>
								ресурсах</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="step-item wow fadeIn" data-wow-delay=".7s">
                    <div class="step-star">
                        <img src="/img/steps/star4.png" alt="">
                    </div>
                    <div class="star-text">
                        Анализируем посещения
                        и <span>улучшаем сайт</span> для
                        пользователей (ПФ)
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="step-item wow fadeIn" data-wow-delay=".8s">
                    <div class="step-star">
                        <img src="/img/steps/star5.png" alt="">
                    </div>
                    <div class="star-text">
                        Уменьшаем показатель
                        <span>отказов</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="form" class="default-block">
    <div class="container">
        <div class="title-section text-center">
            <div class="title-content">
                ознакомьтесь с нашим <span class="yellow wow fadeInRight">коммерческим предложением</span>
            </div>
        </div>
        <form action="" class="form-style ajax-form">
            <div class="col-form wow fadeInUp" >
                <div class="wrapper-input"  data-wow-delay="0.1s"  data-wow-duration="0.5s">
                    <input placeholder="Ваше имя" class="input-style" type="text" name="name">
                </div>
            </div>
            <div class="col-form wow fadeInUp"  data-wow-delay="0.3s" data-wow-duration="0.5s">
                <div class="wrapper-input">
                    <input placeholder="Ваш телефон" class="input-style" type="text" name="phone">
                </div>
            </div>
            <div class="col-form wow fadeInUp" data-wow-delay="0.5s"  data-wow-duration="0.5s">
                <div class="wrapper-input wrapper-input-submit">
                    <button type="submit">
                        Получить коммерческое
                    </button>
                </div>
            </div>
        </form>
        <div class="medusa-form wow fadeInUp" data-wow-delay="0.5s">
            <img src="/img/medusa.png" alt="">
        </div>
    </div>
</div>
<div id="reviews" class="default-block">
    <div class="container">
        <div class="title-section">
            <div class="title-content">
                ОТЗЫВЫ
            </div>
        </div>
        <div class="row row-reviews">
            <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s" data-wow-duration="0.5s">
                <div class="slick-reviews">
                    <div class="review-item">
                        <a href="/img/review.jpg"  data-fancybox="Отзывы">
                            <img src="/img/review.jpg" alt="">
                        </a>
                    </div>
                    <div class="review-item">
                        <a href="/img/review.jpg"  data-fancybox="Отзывы">
                            <img src="/img/review.jpg" alt="">
                        </a>
                    </div>
                    <div class="review-item">
                        <a href="/img/review.jpg"  data-fancybox="Отзывы">
                            <img src="/img/review.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.3s" data-wow-duration="0.5s">
                <div class="slick-video">
                    <div class="video-item">
                        <div class="lazyYT" data-youtube-id="_oEA18Y8gM0" data-ratio="16:9"></div>
                    </div>
                    <div class="video-item">
                        <div class="lazyYT" data-youtube-id="_oEA18Y8gM0" data-ratio="16:9"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <a href="/reviews" class="btn skewed-button yellow-button">
                <span>Все отзывы</span>
            </a>
        </div>
    </div>
    <div class="kit wow fadeIn" data-wow-duration="2s">
        <img src="/img/kit.png" alt="">
    </div>
</div>
<div id="clients" class="default-block">
    <div class="container">
        <div class="title-section">
            <div class="title-content">
                Более <div class="yellow wow fadeInRight">&nbsp;300 клиентов</div>
            </div>
        </div>
        <div class="client-slider">
            <div class="client-item">
                <div class="client-img">
                    <img src="/img/yandex.png" alt="">
                </div>
            </div>
            <div class="client-item">
                <div class="client-img">
                    <img src="/img/yandex.png" alt="">
                </div>
            </div>
            <div class="client-item">
                <div class="client-img">
                    <img src="/img/yandex.png" alt="">
                </div>
            </div>
            <div class="client-item">
                <div class="client-img">
                    <img src="/img/yandex.png" alt="">
                </div>
            </div>
            <div class="client-item">
                <div class="client-img">
                    <img src="/img/yandex.png" alt="">
                </div>
            </div>
            <div class="client-item">
                <div class="client-img">
                    <img src="/img/yandex.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="faq" class="default-block">
    <div class="container">
        <div class="title-section">
            <div class="title-content">
                FAQ
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="faq-wrapper">
                    <div class="faq-question wow fadeInUp" data-wow-delay="0.3s">
                        Сколько стоит продвижение в моем регионе
                    </div>
                    <div class="faq-answer">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente fuga, omnis, alias laudantium eius quod doloribus labore, nisi, magni dolorem accusantium quo vitae repellendus rerum. Hic fuga, ad eaque expedita!
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="faq-wrapper">
                    <div class="faq-question wow fadeInUp" data-wow-delay="0.2s">
                        Сколько стоит продвижение в моем регионе
                    </div>
                    <div class="faq-answer">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente fuga, omnis, alias laudantium eius quod doloribus labore, nisi, magni dolorem accusantium quo vitae repellendus rerum. Hic fuga, ad eaque expedita!
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="faq-wrapper">
                    <div class="faq-question wow fadeInUp" data-wow-delay="0.5s">
                        Сколько стоит продвижение в моем регионе
                    </div>
                    <div class="faq-answer">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente fuga, omnis, alias laudantium eius quod doloribus labore, nisi, magni dolorem accusantium quo vitae repellendus rerum. Hic fuga, ad eaque expedita!
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="faq-wrapper">
                    <div class="faq-question wow fadeInUp" data-wow-delay="0.15s">
                        Сколько стоит продвижение в моем регионе
                    </div>
                    <div class="faq-answer">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente fuga, omnis, alias laudantium eius quod doloribus labore, nisi, magni dolorem accusantium quo vitae repellendus rerum. Hic fuga, ad eaque expedita!
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="faq-wrapper">
                    <div class="faq-question wow fadeInUp" data-wow-delay="0.25s">
                        Сколько стоит продвижение в моем регионе
                    </div>
                    <div class="faq-answer">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente fuga, omnis, alias laudantium eius quod doloribus labore, nisi, magni dolorem accusantium quo vitae repellendus rerum. Hic fuga, ad eaque expedita!
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="faq-wrapper">
                    <div class="faq-question wow fadeInUp" data-wow-delay="0.17s">
                        Сколько стоит продвижение в моем регионе
                    </div>
                    <div class="faq-answer">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente fuga, omnis, alias laudantium eius quod doloribus labore, nisi, magni dolorem accusantium quo vitae repellendus rerum. Hic fuga, ad eaque expedita!
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <button data-toggle="modal" data-target="#modal-contact" class="btn skewed-button yellow-button">
                <span>Задать вопрос</span>
            </button>
        </div>
    </div>
</div>
<div id="congrats">
    <div class="container">
        <div class="title-section">
            <div class="title-content">
                Поздравляем!
            </div>
        </div>
        <div class="congrats-block row">
            <div class="offset-md-6 col-md-6">
                <div class="congrats-title">
                    Вы дошли до самого конца <br>
                    и нашли  золотую монету  <br>
                    и получаете <div class="yellow bebas wow flash d-inline-block">7% скидку!</div>
                </div>
                <button class="gradient-btn px-5 button" data-toggle="modal" data-target="#modal-contact">
                    <span>Получить скидку</span>
                </button>
            </div>
        </div>
        <div class="aboat">
            <img src="/img/batiskaf.png" class="batiskaf" alt="">
            <img src="/img/lights.png" class="lights" alt="">
        </div>
    </div>
</div>