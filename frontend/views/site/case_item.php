
<div class="page-wrapper">
    <div class="container">
        <h1 class="title-page">
            Выполненные работы (кейсы)
        </h1>
        <ul class="breadcrumb">
            <li>
                <a href="/">Главная</a>
            </li>
            <li>
                <a href="/blog">Блог</a>
            </li>
            <li>
					<span>
                        <?= $portfolio->title ?>
					</span>
            </li>
        </ul>
        <div class="back-to-blog">
            <a href="/cases/<?= $portfolio->category ?>" class="btn skewed-button yellow-button">
                <span>< Все кейсы</span>
            </a>
        </div>
        <div class="blog-wrapper">
            <div class="blog-header">
                <div class="blog-title">
                    <?= $portfolio->title ?>
                </div>
                <div class="blog-year">
                    Год исполнения: <?= $portfolio->year ?>
                </div>
            </div>
            <div class="blog-content">
                <?= $blog->description ?>
            </div>
        </div>
        <div class="text-right">
            <a href="/cases/<?= $portfolio->category ?>" class="btn skewed-button yellow-button">
                <span>< Все кейсы</span>
            </a>
        </div>
    </div>
</div>
<?= $this->render('/layouts/form') ?>