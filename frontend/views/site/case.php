<?php
    $this->title = 'Кейсы';
    $category = Yii::$app->request->get()['category'];

use yii\widgets\LinkPager; ?>
	<div class="page-wrapper">
		<div class="container">
			<h1 class="title-page">
			 Выполненные работы (кейсы)
			</h1>
			<ul class="breadcrumb">
				<li>
					<a href="#">Главная</a>
				</li>
				<li>
					<span>Кейсы</span>
				</li>
			</ul>
			<div id="cases">
				<ul class="nav tabs" role="tablist">
				  <li class="nav-item">
				    <a class="nav-link <?= $category == '1' ? 'active' : '' ?>" href="/cases/1"  role="tab" aria-selected="true">Товары</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link  <?= $category == '2' ? 'active' : '' ?>" href="/cases/2"  role="tab" aria-selected="true">Услуги</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link  <?= $category == '3' ? 'active' : '' ?>" href="/cases/3" role="tab" aria-selected="true">Производство</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link  <?= $category == '4' ? 'active' : '' ?>" href="/cases/4" role="tab" aria-selected="true">Прочее</a>
				  </li>
				</ul>
				  	<div class="portfolio-row">
                        <? foreach ($models as $item): ?>
				  		<div class="portfolio-col">
				  			<div class="portfolio-item">
				  				<div class="portfolio-head">
				  					<div class="portfolio-name">
                                        <a href="/case/<?= $item->url ?>"><?= $item->title ?></a>
				  					</div>
				  					<div class="portfolio-year">
                                        <?= $item->year ?>
				  					</div>
				  				</div>
				  				<div class="portfolio-content">
				  					<div class="portfolio-info">
				  						<div class="portfolio-info-col">
					  						<div class="portfolio-info-title">
					  							Сайт
					  						</div>
					  						<div class="portfolio-info-value">
                                                <?= $item->site_url ?>
					  						</div>
				  						</div>
				  						<div class="portfolio-info-col">
					  						<div class="portfolio-info-title">
					  							Тема
					  						</div>
					  						<div class="portfolio-info-value">
                                                <?= $item->theme ?>
					  						</div>
				  						</div>
				  						<div class="portfolio-info-col">
					  						<div class="portfolio-info-title">
					  							Было
					  						</div>
					  						<div class="portfolio-info-value-number">
					  							<span class="">
                                                <?= $item->before ?>
					  							</span>
					  							пользователей<br>
					  							в день
					  						</div>
				  						</div>
				  						<div class="portfolio-info-col">
					  						<div class="portfolio-info-title">
					  							Стало
					  						</div>
					  						<div class="portfolio-info-value-number">
					  							<span class="">
                                                <?= $item->after ?>
					  							</span>
					  							пользователей<br>
					  							в день
					  						</div>
				  						</div>
				  					</div>
				  					<div class="portfolio-end">
				  						Итого рост посетителей <span class="yellow">
                                                <?= $item->total ?></span>
				  					</div>
				  				</div>
				  			</div>
				  		</div>
                        <? endforeach ?>
				  	</div>

                <? echo LinkPager::widget([
                'pagination' => $pages,
                ]);
                ?>
			</div>
		</div>
		
				<div class="shark">
					<img src="/img/shark.png" alt="">
				</div>
	</div>
