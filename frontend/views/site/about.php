<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О нас';
?>

	<div class="page-wrapper">
		<div class="container">
			<h1 class="title-page">
				О нас
			</h1>
			<ul class="breadcrumb">
				<li>
					<a href="/">Главная</a>
				</li>
				<li>
					<span>О нас</span>
				</li>
			</ul>
			<div class="about-us-block">
				<div class="about-us-left">
					<iframe height="315" src="https://www.youtube.com/embed/_xW_58mRrSk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<div class="about-us-text">
						<p
						>Наша компания вышла на орбиту рынка в далеком 
						2010 году как космическая ракета. Мы с первых дней 
						поставили себе цель выбиться в лидеры и делали все, 
						что от нас зависело.
						</p>
						<p>
						К каждому проекту мы относимся как к собственному, 
						вкладывая силы,знания ,навыки в его успешную 
						реализацию.Ценим довери клиента и всегда выполняем 
						взятые на себя обязательства
						</p>
				</div>
			</div>
			<div class="team-slider">
				<div class="team-item">
					<div class="team-image">
						<img src="/img/company/1.jpg" alt="">
					</div>
					<div class="team-info">
						<div class="team-name">
								Тимур Нурлихин
						</div>
						<div class="team-position">
							Капитан корабля
						</div>
					</div>
				</div>
				<div class="team-item">
					<div class="team-image">
						<img src="/img/company/2.jpg" alt="">
					</div>
					<div class="team-info">
						<div class="team-name">
							 Игорь Быков
						</div>
						<div class="team-position">
							 Технический директор
						</div>
					</div>
				</div>
				<div class="team-item">
					<div class="team-image">
						<img src="/img/company/10.jpg" alt="">
					</div>
					<div class="team-info">
						<div class="team-name">
							 Эдуард Ау
						</div>
						<div class="team-position">
							СЕО-специалист
						</div>
					</div>
				</div>
				<div class="team-item">
					<div class="team-image">
						<img src="/img/company/7.jpg" alt="">
					</div>
					<div class="team-info">
						<div class="team-name">
								Сухраб Садыков
						</div>
						<div class="team-position">
								Веб-программист
						</div>
					</div>
				</div>
				<div class="team-item">
					<div class="team-image">
						<img src="/img/company/3.jpg" alt="">
					</div>
					<div class="team-info">
						<div class="team-name">
							Меирлан Темиров
						</div>
						<div class="team-position">
							Менеджер
						</div>
					</div>
				</div>
			</div>
			<div class="company-group-container">
				<div class="company-group bb-background2">
					<div class="row">
						<div class="col-md-3 company-group__main-logo">
							<a href="https://netlight.kz" title="netlight.kz"><img src="/img/netlight.png" alt="netlight.kz" /></a>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-12">
									<p>Проект Nasa.kz входит в группу проектов Digital агентства Netlight.kz.
									</p>
									<p><b>Другие проекты Netlight:</b></p>
									<div class="row">
										<div class="col-6 col-md-2 company-group__logo">
											<a href="https://edumap.kz" title="edumap.kz"><img src="/img/logos/edumap.png" alt="edumap.kz" /></a>
										</div>
										<div class="col-6 col-md-2 company-group__logo">
											<a href="https://netlight.kz" title="smmgods.kz"><img src="/img/logos/smmgods.png" alt="smmgods.kz" /></a>
										</div>
										<div class="col-6 col-md-2 company-group__logo">
											<a href="https://netlight.kz" title="nasa.kz"><img src="/img/logos/nasa.png" alt="goldencoin.kz" /></a>
										</div>
										<div class="col-6 col-md-2 company-group__logo">
											<a href="https://netlight.kz" title="ltv.kz"><img src="/img/logos/ltv.png" alt="ltv.kz" /></a>
										</div>
										<div class="col-6 col-md-2 company-group__logo">
											<a href="https://netlight.kz" title="playmedia.kz"><img src="/img/logos/playmedia.png" alt="playmedia.kz" /></a>
										</div>
										<div class="col-6 col-md-2 company-group__logo">
											<a href="https://netlight.kz" title="hotbrand.kz"><img src="/img/logos/hotbrand.png" alt="hotbrand.kz" /></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

			<div class="medusa-form wow fadeInUp" data-wow-delay="0.5s">
				<img src="/img/medusa.png" alt="">
			</div>
				</div>
			</div>
		</div>
	</div>
<?= $this->render('/layouts/form') ?>