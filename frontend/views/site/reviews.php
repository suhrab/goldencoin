<?php
    $this->title = 'Отзывы'
?>
	<div class="page-wrapper">
		<div class="container">
			<h1 class="title-page">
				О нас
			</h1>
			<ul class="breadcrumb">
				<li>
					<a href="#">Главная</a>
				</li>
				<li>
					<span>Отзывы</span>
				</li>
			</ul>
			<div class="video-reviews-page">
				<div class="video-review-item">
						<div class="lazyYT" data-youtube-id="_oEA18Y8gM0" data-ratio="16:9"></div>
				</div>
				<div class="video-review-item">
						<div class="lazyYT" data-youtube-id="_oEA18Y8gM0" data-ratio="16:9"></div>
				</div>
				<div class="video-review-item">
						<div class="lazyYT" data-youtube-id="_oEA18Y8gM0" data-ratio="16:9"></div>
				</div>
				<div class="video-review-item">
						<div class="lazyYT" data-youtube-id="_oEA18Y8gM0" data-ratio="16:9"></div>
				</div>
			</div>
			<div class="reviews-images-row">
				<div class="reviews-image-col">
					<div class="reviews-image">
						<a href="/img/review.jpg" data-fancybox="Отзыв">
							<img src="/img/review.jpg" alt="">
						</a>
						<div class="plus">
							<span></span><span></span>
						</div>
					</div>
				</div>
				<div class="reviews-image-col">
					<div class="reviews-image">
						<a href="/img/review.jpg" data-fancybox="Отзыв">
							<img src="/img/review.jpg" alt="">
						</a>
						<div class="plus">
							<span></span><span></span>
						</div>
					</div>
				</div>
				<div class="reviews-image-col">
					<div class="reviews-image">
						<a href="/img/review.jpg" data-fancybox="Отзыв">
							<img src="/img/review.jpg" alt="">
						</a>
						<div class="plus">
							<span></span><span></span>
						</div>
					</div>
				</div>
				<div class="reviews-image-col">
					<div class="reviews-image">
						<a href="/img/review.jpg" data-fancybox="Отзыв">
							<img src="/img/review.jpg" alt="">
						</a>
						<div class="plus">
							<span></span><span></span>
						</div>
					</div>
				</div>
				<div class="reviews-image-col">
					<div class="reviews-image">
						<a href="/img/review.jpg" data-fancybox="Отзыв">
							<img src="/img/review.jpg" alt="">
						</a>
						<div class="plus">
							<span></span><span></span>
						</div>
					</div>
				</div>
				<div class="reviews-image-col">
					<div class="reviews-image">
						<a href="/img/review.jpg" data-fancybox="Отзыв">
							<img src="/img/review.jpg" alt="">
						</a>
						<div class="plus">
							<span></span><span></span>
						</div>
					</div>
				</div>
				<div class="reviews-image-col">
					<div class="reviews-image">
						<a href="/img/review.jpg" data-fancybox="Отзыв">
							<img src="/img/review.jpg" alt="">
						</a>
						<div class="plus">
							<span></span><span></span>
						</div>
					</div>
				</div>
				<div class="reviews-image-col">
					<div class="reviews-image">
						<a href="/img/review.jpg" data-fancybox="Отзыв">
							<img src="/img/review.jpg" alt="">
						</a>
						<div class="plus">
							<span></span><span></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?= $this->render('/layouts/form') ?>
