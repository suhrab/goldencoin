<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GOLDEN COIN</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/animate.css">
	<link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="/css/slick.css">
	<link rel="stylesheet" href="/css/jquery.fancybox.min.css">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/jquery.fancybox.min.css">
	<link rel="stylesheet" href="/css/lazyYT.css">
</head>
<body class="page404">
	<div class="fixed-menu">
		<div class="wrapper-menu">
			<div class="menu-items">
				<div>
					<a href="">Главная</a>
				</div>
				<div>
					<a href="">О нас</a>
				</div>
				<div>
					<a href="">Кейсы</a>
				</div>
				<div>
					<a href="">Отзывы</a>
				</div>
				<div>
					<a href="">Блог</a>
				</div>
				<div>
					<a href="">Контакты</a>
				</div>
			</div>
		</div>
		<div class="close-menu">
			<img src="/img/close_img.png" alt="">
		</div>
	</div>
	<div class="fixed-menu-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-7 col-left-header">
					<div class="burger-menu-top">
						<img src="/img/burger.png" alt="">
					</div>
					<a href="" class="logo">
						<img src="img/logo_black.png" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>	

	<div id="error404">
		<img src="/img/404.png" class="animated zoomIn" alt="">
			<div class="title-error">
				Страница не найдена
			</div>
			<a class="blue-gradient-btn" href="/">Назад</a>
	</div>

	<script src="/js/jquery-3.4.1.min.js"></script>
	<script src="/js/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/slick.min.js"></script>
	<script src="/js/jquery.fancybox.min.js"></script>
	<script src="/js/lazyYT.js"></script>
	<script src="/js/jquery.mask.min.js"></script>
	<script src="/js/common.js"></script>
</body>
</html>