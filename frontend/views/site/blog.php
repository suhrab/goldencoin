<?php
    use yii\widgets\LinkPager;
?>

<? $this->title = 'Блог' ?>
<div class="page-wrapper">
    <div class="container">
        <h1 class="title-page">
            Блог
        </h1>
        <ul class="breadcrumb">
            <li>
                <a href="#">Главная</a>
            </li>
            <li>
					<span>
						Блог
					</span>
            </li>
        </ul>
        <div class="blog-items">
            <? foreach ($models as $model): ?>
            <div class="blog-item-summary">
                <div class="blog-header">
                    <div class="blog-title">
                        <a href="/blog/<?= $model->url ?>"><?= $model->title ?></a>
                    </div>
                    <div class="blog-year">
                        <?= date('d.m.Y',$model->created_at) ?>
                    </div>
                </div>
                <div class="blog-content">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="blog-img">
                                <img src="/uploads/blog/<?= $model->image ?>" alt="">
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="blog-content-summary">
                                <?= $model->summary ? $model->summary : mb_substr(strip_tags($model->description),0,300) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <? endforeach ?>
            <? echo LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>
        </div>
    </div>
</div>

<?= $this->render('/layouts/form') ?>