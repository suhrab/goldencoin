<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "portfolio".
 *
 * @property int $id
 * @property string $title
 * @property string $site
 * @property int|null $category
 * @property string|null $year
 * @property string|null $site_url
 * @property string|null $theme
 * @property string|null $before
 * @property string|null $after
 * @property string|null $total
 * @property int|null $sort
 * @property string|null $images
 */
class Portfolio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portfolio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'site_url'], 'required'],
            [['category', 'sort'], 'integer'],
            [['images','url','title_SEO','description_SEO','keywords_SEO','description'], 'string'],
            [['title', 'site', 'year', 'site_url', 'theme', 'before', 'after', 'total'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'site' => 'Сайт',
            'category' => 'Категория',
            'year' => 'Дата',
            'site_url' => 'Сайт ',
            'theme' => 'Тема',
            'before' => 'До',
            'after' => 'После',
            'total' => 'Итог',
            'sort' => 'Сортировка',
            'images' => 'Картинки',
            'url'   => 'URL'
        ];
    }

    public function getImageUrl()
    {
        return '/uploads/web/'.$this->images;
    }
}
