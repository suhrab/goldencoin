<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string|null $summary
 * @property string|null $image
 * @property int|null $created_at
 * @property string|null $title_SEO
 * @property string|null $description_SEO
 * @property string|null $keywords_SEO
 * @property string|null $url
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description', 'summary'], 'string'],
            [['created_at'], 'integer'],
            [['title', 'title_SEO', 'description_SEO', 'keywords_SEO', 'url'], 'string', 'max' => 255],
            [['url'], 'unique'],
            ['image','file']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Контент',
            'summary' => 'Краткое описание',
            'image' => 'Картинка',
            'created_at' => 'Created At',
            'title_SEO' => 'Заголовок(SEO)',
            'description_SEO' => 'Описание(SEO)',
            'keywords_SEO' => 'Ключи(SEO)',
            'url' => 'Url',
        ];
    }
}
