<?php

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portfolio-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'category')->dropDownList([
                    '1' => 'Товары',
                    '2' => 'Услуги',
                    '3' => 'Производство',
                    '4' => 'Прочее',
            ]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'site_url')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-lg-12">
            <?=
            $form->field($model, 'description')->widget(CKEditor::class,[
                'editorOptions' => ElFinder::ckeditorOptions(
                    'elfinder'
                )
            ]);
            ?>

        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'theme')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'before')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'after')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'total')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-lg-6">
            <?= $form->field($model, 'title_SEO')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'description_SEO')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'keywords_SEO')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?
    echo $form->field($model, 'images')->widget(FileInput::class, [
                                'options' => ['accept' => 'image/*','multiple'=> true],
                                'language'  => 'ru',
                                'pluginOptions' => [
                                        'initialPreview'        => $path_images ? $path_images : false,
                                        'initialPreviewAsData'  => true,
                                        'initialCaption'        => $model->images,
                                        'showCancel'    => false,
                                        'showUpload'    => false,
                                        'browseLabel'   => 'Выбрать изображение',
                                ],
                            ]);
?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
