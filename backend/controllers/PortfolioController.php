<?php

namespace backend\controllers;

use common\models\Helper;
use Yii;
use common\models\Portfolio;
use common\models\PortfolioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PortfolioController implements the CRUD actions for Portfolio model.
 */
class PortfolioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Portfolio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PortfolioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Portfolio model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Portfolio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Portfolio();

        if ($model->load(Yii::$app->request->post())) {
            $model->url = $model->url ? $model->url : Helper::translit($model->title);
            $images = UploadedFile::getInstances($model,'images');
            foreach ($images as $image)
            {
                $image->saveAs(Yii::getAlias('@frontend'.'/web/uploads/portfolio/'.$image->name));
                $images_serialize[] = $image->name;
            }
            if ($images_serialize)
                $model->images = serialize($images_serialize);
            else
                $model->images = '';

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Portfolio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $saved_images = $model->images;
        $model->images = unserialize($model->images);
        if ($model->images)
        foreach ($model->images as $image_return)
        {
            $path_images[] = '/uploads/portfolio/'.$image_return;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->url = $model->url ? $model->url : Helper::translit($model->title);

            $images = UploadedFile::getInstances($model,'images');
            foreach ($images as $image)
            {
                $image->saveAs(Yii::getAlias('@frontend'.'/web/uploads/portfolio/'.$image->name));
                $images_serialize[] = $image->name;
            }
            if ($images_serialize)
            {
                $model->images = serialize($images_serialize);
            }
            else
            {
                $model->images = $saved_images;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'path_images'   => $path_images
        ]);
    }

    /**
     * Deletes an existing Portfolio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Portfolio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Portfolio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Portfolio::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
