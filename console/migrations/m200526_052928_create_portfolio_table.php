<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%portfolio}}`.
 */
class m200526_052928_create_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%portfolio}}', [
            'id'        => $this->primaryKey(),
            'title'     => $this->string(255)->notNull(),
            'site'      => $this->string(255)->notNull(),
            'category'  => $this->integer(11),
            'year'      => $this->string(255),
            'site_url'  => $this->string(255),
            'theme'     => $this->string(255),
            'before'    => $this->string(255),
            'after'     => $this->string(255),
            'total'     => $this->string(255),
            'sort'      => $this->integer(11),
            'images'    => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%portfolio}}');
    }
}
