<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%blog}}`.
 */
class m200526_040324_create_blog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%blog}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'description'   => $this->text(),
            'summary'   => $this->text(),
            'image' => $this->string(255),
            'created_at'    => $this->integer(11),
            'title_SEO' => $this->string(255),
            'description_SEO' => $this->string(255),
            'keywords_SEO' => $this->string(255),
            'url'   => $this->string(255)->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%blog}}');
    }
}
